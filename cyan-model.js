const START = 'START'
const HERE = 'HERE'
const INVENTORY = 'INVENTORY'
const CURRENT = '_current'
const BEEN = '_been'
const EMPTY = { [INVENTORY]: [], [BEEN]: [], [CURRENT]: START }

function empty() {
    return JSON.parse(JSON.stringify(EMPTY))
}

function hash(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed
    for (let i = 0, ch; i < str.length; i++) {
        ch = str.charCodeAt(i)
        h1 = Math.imul(h1 ^ ch, 2654435761)
        h2 = Math.imul(h2 ^ ch, 1597334677)
    }
    h1 = Math.imul(h1 ^ (h1>>>16), 2246822507) ^ Math.imul(h2 ^ (h2>>>13), 3266489909)
    h2 = Math.imul(h2 ^ (h2>>>16), 2246822507) ^ Math.imul(h1 ^ (h1>>>13), 3266489909)
    return (4294967296 * (2097151 & h2) + (h1>>>0)).toString(16)
}

function isInt(value) {
    var x;
    if (isNaN(value)) {
      return false;
    }
    x = parseFloat(value);
    return (x | 0) === x;
}

function compare(a, b) {
    if (a == b) return true
    if (typeof a == 'number') return a == parseInt(b)
    return false
}

function predicate(condition) {
    let fn
    if (condition.startsWith('?')) { // variable
        let token = condition.substring(1).trim()
        let sep = token.indexOf(' ')
        if (sep > -1) {
            varname = token.substring(0,sep)
            value = token.substring(pos+1).trim()
            fn = (vars) => compare(vars[token], value)
        } else {
            fn = (vars) => vars[token]
        }
    } else if (condition.startsWith('^')) {
        let token = condition.substring(1).trim()
        fn = (vars) => vars[BEEN].includes(token)
    } else {
        let token = condition
        fn = (vars) => vars[INVENTORY].includes(token)
    }
    return fn
}

class Effect {
    constructor(type, tail) {
        switch(type) {
            case '+':
                this.execute = (variables) => {
                    if (!variables[INVENTORY].includes(tail)) {
                        variables[INVENTORY].push(tail)
                    }
                }
                break
            case  '-':
                this.execute = (variables) => {
                    variables[INVENTORY] = variables[INVENTORY].filter(i =>i != tail)
                }
                break
            case '!':
                this.execute = (variables) => {
                    variables = empty()
                }
                break
            case '@':
                if (tail.match(/->/)) {
                    var parts = tail.split('->')
                    var name = parts[0].trim()
                    var value = parts[1].trim()
                    this.execute = (variables) => {
                        if (isNaN(value)) {
                            variables[name] = value
                        } else {
                            let n = parseInt(value)
                            if (isNaN(n)) {
                                variables[name] = value
                            } else {
                                variables[name] = n
                            }
                        }
                    }
                } else if (tail.match(/\+/)) {
                    var parts = tail.split('+')
                    var name = parts[0].trim()
                    var value = parseInt(parts[1].trim())
                    this.execute = (variables) => {
                        variables[name] = (variables[name] || 0) + value
                    }
                } else if (tail.match(/-/)) {
                    var parts = tail.split('-')
                    var name = parts[0].trim()
                    var value = parseInt(parts[1].trim())
                    this.execute = (variables) => {
                        variables[name] = (variables[name] || 0) - value
                    }
                }
                break
            case '*':
                this.execute = (variables) => {
                    variables._MESSAGE = tail
                }
                break
        }
    }
}

class Option {
    constructor(choice) {
        this.choice = choice
        this.routes = []
        this.visible = true
        this.key = hash(this.choice)
    }

    addRoute(gate, dest) {
        var route = {gate: gate, dest: dest, effects: []}
        this.routes.push(route)
        return this
    }

    addEffect(type, value) {
        this.routes[this.routes.length-1].effects.push(new Effect(type, value))
        return this
    }

    clone(visible) {
        var ret = new Option(this.choice)
        ret.destinations = this.destinations
        ret.routes = this.routes
        ret.key = this.key
        ret.parentkey = this.parentkey
        ret.visible = visible
        return ret
    }
}


const DFL_KEY = '_'
class Block {
    constructor(name, base) {
        this.name = name
        this.key = hash(name)
        this._bodies = []
        this.defaultBody = null
        this.effects = []

        if (base) {
            this.options = base.options
            this.variables = base.variables
        } else {
            this.options = {}
            this.variables = {}
        }
    }

    addBody(body, condition) {
        if (!condition) {
            this.defaultBody = body
            return this
        }

        let fn = predicate(condition)
        this._bodies.push({test: fn, body: body.trim()})
        return this
    }

    getBody(variables) {
        let ret = this.defaultBody
        if (variables) {
            for (var i in this._bodies) {
                var body = this._bodies[i]
                var found = body.test(variables)
                if (found) {
                    ret = body.body
                }
            }
        }

        return ret
    }

    addEffect(type, value) {
        this.effects.push(new Effect(type, value))
        return this
    }

    addOption(option) {
        this.options[`${option.parentkey}/${option.key}`] = option
        return this
    }

    set(name, value) {
        this.variables[name] = value
    }

    get(name) {
        return this.variables[name]
    }
}

class Game {
    constructor(name, debug) {
        this.name = name
        this.items = {}
        this._pages = {}
        this.debug = debug
    }

    current() {
        return this.page
    }

    next(variables, choice) {
        let current = variables[CURRENT]
        let page = this.getPage(current)
        let been = variables[BEEN]
        let inventory = variables[INVENTORY]

        var option = page.options[choice]
        if (!option) {
            variables._ERROR = `unrecognised choice '${choice}''`
            return current
        }

        var path

        for (var i in option.routes) {
            var route = option.routes[i]
            if (!route.gate) {
                if (route == HERE) {
                    path = current
                } else {
                    path = route
                }
                break
            }
            if (route.gate.startsWith('@')) {
                var varname = route.gate.substring(1)
                if (variables[varname]) {
                    path = route
                    break
                }
            } else if (route.gate.startsWith('^')) {
                var pagename = route.gate.substring(1)
                if (been.includes(pagename)) {
                    path = route
                    break
                }
            } else {
                if (inventory.includes(route.gate)) {
                    path = route
                    break
                }
            }
        }

        if (!path) {
            variables._ERROR = `no valid route for '${choice}''`
            return current
        }

        if (!this.hasPage(path.dest)) {
            variables._ERROR = `no definition for page '${path.dest}''`
            return current
        }

        for (var i in path.effects) {
            let effect = path.effects[i]
            effect.execute(variables)
        }

        return path.dest
    }

    getPage(pagename) {
        return this._pages[pagename]
    }

    hasPage(pagename) {
        return pagename == HERE || pagename in this._pages
    }

    allPages() {
        return this._pages
    }

    addItem(name, page) {
        this.items[name] = page
        return this
    }

    addVariable(name, initial) {
        this.variables[name] = initial
        return this
    }

    addBlock(name, page) {
        this._pages[name] = page
        return this
    }
}

function dumpOption(opt) {
    console.log(`|  option: ${opt.choice}`)
    for (r in opt.routes) {
        var route = opt.routes[r]
        console.log(`|    route ${route.gate} -> ${route.dest}`)
        for (e in route.effects) {
            var effect = route.effects[e]
            console.log(`|      effect ${JSON.stringify(effect)}`)
        }
    }
}

function dumpBlock(block, type) {
    console.log(`| ${type}: ${block.name}`)
    console.log(`${JSON.stringify(block.body)}`)
    for (i in block.options) {
        var opt = block.options[i]
        dumpOption(opt)
    }
}

function dump(game) {
    console.log(`==================`)
    console.log(`Game: ${game.name}`)
    for (p in game.allPages()) {
        dumpBlock(game.getPage(p), 'Block')
    }
    for (p in game.items) {
        dumpBlock(game.items[p], 'Item')
    }
}

module.exports = {
    Effect : Effect,
    Option : Option,
    Block : Block,
    Game : Game,
    dumpOption : dumpOption,
    dumpBlock : dumpBlock,
    dump : dump,
    hash: hash,
    empty: empty,
    INVENTORY : INVENTORY,
    CURRENT : CURRENT,
    BEEN: BEEN,
    EMPTY : EMPTY,
    START : START,
    HERE : HERE
}
