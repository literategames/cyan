import {Remarkable} from 'remarkable'

import cyan, { START, CURRENT, BEEN, INVENTORY, HERE, EMPTY } from "./cyan-model"
import load from "./cyan-loader"

function dfl_main(main_text) {
    var body = document.getElementById('_body')
    body.innerHTML = main_text
}

function dfl_item(item) {
    var item_root = document.getElementById('_items')
    var element = document.getElementById('item_proto').cloneNode(true)
    element.classList.add(`item_${item.key}`)
    element.classList.add(`added`)
    element.classList.add(`flippable`)
    var item_name = element.getElementsByClassName('item_name_proto')[0]
    item_name.id = `item_${item.key}_name`
    item_name.classList.remove('item_name_proto')
    item_name.innerHTML = slashPrefix(item.name)
    var item_desc = element.getElementsByClassName('item_desc_proto')[0]
    item_desc.id = `item_${item.key}_desc`
    item_desc.classList.remove('item_desc_proto')
    item_desc.innerHTML = item.formatted
    item_root.appendChild(element)
}

function dfl_opt(option) {
    var option_root = document.getElementById('_options')
    var element = document.getElementById('option_proto').cloneNode(true)
    element.classList.add(`added`)
    element.removeAttribute('id')
    if (option.flippable) {
        element.classList.add(`flippable`)
    }
    var contents = `<a class="choice" href="javascript:window.story.act('${option.parentkey}/${option.key}')">${option.choice}</a>`
    //TODO add error message if available
    //TODO do I need a hash for the class add below?
    element.innerHTML = contents
    if (option.visible) {
        element.style='display:list-item'
    } else {
        element.classList.add(`item_${option.parentkey}`)
    }
    option_root.appendChild(element)
}

function dfl_inv(inventory_text) {
    var inv_root = document.getElementById('_inventory')
    var inv = document.getElementById('inventory_proto').cloneNode(true)
    inv.classList.add(`added`)
    inv.innerHTML = inventory_text
    inv.style='display:list-item'
    inv_root.appendChild(inv)
}

function dfl_markdown(s) {
    return md.render(s)
}

function hide(x) {
    x.style.display = "none";
}

function show(x) {
    var tag = x.tagName
    switch(tag) {
        case 'LI':
            x.style.display = 'list-item'
            break;
        default:
            x.style.display = 'block'
            break;
    }
}

function flip(x) {
    if (x.style.display === "none") {
        show(x)
    } else {
        hide(x)
    }
}


function addUnique(collection, value) {
    if (!collection.includes(value)) {
        collection.push(value)
    }
}

function resolvePage(variables, name) {
    if (name == HERE) {
        return variables[CURRENT]
    }
    return name
}

function validate(game, option, current) {
    var error = ''
    for (i in option.routes) {
      var route = option.routes[i]
      if (!game.hasPage(route.dest)) {
        var message = `no destination page ${route.dest} for choice ${option.choice} on page ${current}`
        error += message
        console.log(message)
      }
    }
    if (error != '') {
      option.broken = error
    }
  }

function expand(text, variables) {
    return text.replace(/\{[^\}]+\}/g, function(s) {
      var key = s.substring(1,s.length-1)
      var ret = variables[key]
      return ret
    })
}

function filter(obj, predicate) {
    return Object.keys(obj)
          .filter( key => predicate(key) )
          .reduce( (res, key) => (res[key] = obj[key], res), {} );
}

function clear(classname) {
    var added = document.getElementsByClassName(classname)
    while(added[0]) {
        added[0].parentNode.removeChild(added[0])
    }
}

function addCookie(name, value) {
    document.cookie = `${name}=${encodeURIComponent(value)};Secure`
}

function saveable(variables) {
    return filter(variables, key => !key.startsWith('_'))
}

function slashPrefix(key) {
    if (key.includes('/')) {
        return key.substring(0,key.indexOf('/'))
    }
    return key
}

const story = {
    save: function() {
        addCookie(this.pagecookie, this.variables[CURRENT])
        addCookie(this.varcookie, JSON.stringify(saveable(this.variables)))
        addCookie(this.beencookie, JSON.stringify(this.variables[BEEN]))
    },
    restore: function() {
        let cookie_current = null
        let cookie_variables = null
        let cookie_been = null
        let cookies = document.cookie.split(';')

        for (var i in cookies) {
            let cookie = cookies[i].split('=')
            if (cookie.length == 2) {
                let name = cookie[0].trim()
                let value = cookie[1].trim()
                if (name == this.pagecookie) {
                    cookie_current = decodeURIComponent(value)
                } else if (name == this.varcookie) {
                    cookie_variables = JSON.parse(decodeURIComponent(value))
                } else if (name == this.beencookie) {
                    cookie_been = JSON.parse(decodeURIComponent(value))
                }
            }
        }

        this.variables = cookie_variables || cyan.empty()
        this.variables[CURRENT] = cookie_current || START
        this.variables[BEEN] = cookie_been || []
    },
    restart: function() {
        this.variables = cyan.empty()
        this.save()
        this.show()
    },
    jump: function(pagename) {
        pagename = pagename || this.variables[CURRENT] || START
        this.variables[CURRENT] = pagename
        this.save()
        this.show()
    },
    setup: function(handlers) {
        handlers = handlers || {}
        this.main_fn = handlers.main_fn || dfl_main
        this.exam_fn = handlers.exam_fn || dfl_item
        this.opt_fn = handlers.opt_fn || dfl_opt
        this.inv_fn = handlers.inv_fn || dfl_inv
        this.markdown = handlers.markdown || dfl_markdown
        this.md = new Remarkable()
        this.debug = window.config.debug
    },
    load: function(url, callback) {
        load(url, (game) => {
            this.game = game
            this.prefix = `cyan-${cyan.hash(this.game.name)}`
            this.pagecookie = `${this.prefix}-page`
            this.varcookie = `${this.prefix}-vars`
            this.beencookie = `${this.prefix}-been`
            this.restore()
            callback(null, game)
        })
    },
    show: function() {
        let current = this.variables[CURRENT]
        let page = this.game.getPage(current)

        if (! this.variables[BEEN].includes(current)) {
            for (let i in page.effects) {
                let effect = page.effects[i]
                effect.execute(this.variables)
            }
        }

        clear('added')

        this.items = {}

        let substitute = (s) => {
            return this.variables[s.trim()] || 'unknown'
        }

        let examine = (s) => {
            let name = s.replace(/[\[\]]/g,'')
            let key = name
            let showname = name
            if (key.includes(':')) {
                let pos = key.indexOf(':')
                showname = key.substring(0,pos)
                key = key.substring(pos+1).trim()
            } else if (key.includes('/')) {
                showname = key.substring(0,key.indexOf('/'))
            }
            let item = this.game.items[key]
            if (!item) {
                var message = `no definition for item ${key}`
                console.log(message)
                return `<span style='color:red'>${key}</span>`
            }
            item.formatted = expand(this.md.render(item.getBody(this.variables, this.been)), this.variables).
                replace(/\[[^\]]*\]/g, examine).
                replace(/\{[^\}]*\}/g, substitute)
            for (i in item.options) {
                var opt = item.options[i]
                var opt2 = opt.clone(false)
                opt2.flippable = true
                validate(this.game, opt2, this.variables[CURRENT])
                page.addOption(opt2)
            }
            this.items[item.key] = item
            return `<a href="#" onclick="window.story.toggle('${item.key}')">${showname}</a>`
        }

        this.main_fn(expand(this.md.render(page.getBody(this.variables)), this.variables).
            replace(/\[[^\]]*\]/g, examine).
            replace(/\{[^\}]*\}/g, substitute))
        for (var opt in page.options) {
            var option = page.options[opt]
            this.opt_fn(option)
        }
        for (var it in this.items) {
            var item = this.items[it]
            this.exam_fn(item)
        }

        var messagearea = document.getElementById('_message')
        if (this.variables._MESSAGE) {
            var message = document.createElement('b')
            message.classList.add('added')
            message.classList.add('transient')
            message.innerHTML = this.variables._MESSAGE
            messagearea.appendChild(message)
            this.variables._MESSAGE = null
        }

        if (this.variables._ERROR) {
            var message = document.createElement('b')
            message.setAttribute('style', 'color:red')
            message.classList.add('added')
            message.classList.add('transient')
            message.innerHTML = this.variables._ERROR
            messagearea.appendChild(message)
            this.variables._ERROR = null
        }

        for (i in this.variables[INVENTORY]) {
          var it = this.variables[INVENTORY][i]
          this.inv_fn(expand(it, this.variables))
        }

        if (this.game.debug || this.debug) {
            var debug = document.getElementById('developer')

            var visited = document.getElementById('debug_been')
            var beenlist = document.createElement('ul')
            beenlist.classList.add('added')
            beenlist.innerHTML = JSON.stringify(this.variables[BEEN])
            visited.appendChild(beenlist)

            var vars = document.getElementById('debug_vars')
            var varlist = document.createElement('ul')
            varlist.classList.add('added')
            var visible = filter(saveable(this.variables), key => key != INVENTORY)
            for (var key in visible) {
                var v = document.createElement('li')
                v.innerHTML = `${key} => ${JSON.stringify(visible[key])}`
                varlist.appendChild(v)
            }
            vars.appendChild(varlist)

            var pages = document.getElementById('debug_pages')
            var pagelist = document.createElement('ul')
            pagelist.classList.add('added')
            for (var key in this.game.allPages()) {
                let page = this.game.getPage(key)
                let p = document.createElement('li')
                p.innerHTML = `<a href="#" onclick="window.story.jump('${page.name}'); window.story.show()">${key}</a>`
                pagelist.appendChild(p)
            }
            pages.appendChild(pagelist)
            developer.style = 'display:block'
        }

        addUnique(this.variables[BEEN], current)
    },
    act: function(choice) {
        let current = this.variables[CURRENT]
        let next = this.game.next(this.variables, choice)
        current = resolvePage(this.variables, next)
        this.variables[CURRENT] = current

        this.save()
        this.show()
    },
    toggle: function(key) {
        let classname = 'item_'+key
        for (var element of document.getElementsByClassName('flippable')) {
            if (element.classList.contains(classname)) {
                flip(element)
            } else {
                hide(element)
            }
        }
        clear('transient')
    }
}

window.story = story

window.onload = function(e) {
    function loadStory(name, callback) {
        fetch(name, {mode: 'cors'}).then((r)=>{r.text().then(callback)})
    }

    loadStory(window.config.story, (text)=>{
        story.setup()
        story.load(text, (error) => {
            if (error) {
                var block = document.getElementById('error')
                block.innerHTML = error
                block.style ='display:block'
            }
            document.title = `${story.game.name}`
            story.jump(window.config.start)
        })
    })
}
