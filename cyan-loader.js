const cyan = require('./cyan-model')

const NEW = 'NEW'
const COMMAND = 'COMMAND'
const BODY = 'BODY'
const ITEM = 'ITEM'

var mode = NEW
var collectingItem = false

var game = null
var page = null
var option = null
var body = null
var item = null
var callout = null

function reset() {
  mode = NEW
  game = null
  page = null
  option = null
  body = null
  item = null
  callout = null
}

function flushItem() {
    if (item) {
      game.addItem(item, callout)
      item = null
    }
  }

function flushOption() {
  if (option) {
    if (!page) {
      console.log(`can't add option ${option.choice} to unknown page`)
    } else {
      option.parentkey = page.key
      page.addOption(option)
    }
    option = null
  }
}

function flushBody() {
  if (body) {
    page.addBody(body)
    body = null
  }
}

function flushBlock() {
  if (page) {
    if (collectingItem) {
      page.addBody(body)
      game.addItem(page.name, page)
    } else {
      if (page.name.startsWith('(')) {
        if (body) {
          let end = page.name.indexOf(')')
          let predicate = page.name.substring(1,end).trim()
          let name = page.name.substring(end+1).trim()
          let base = game.getPage(name)
          base.addBody(body, predicate)
        }
      } else {
        if (body) {
          page.addBody(body)
        }
        game.addBlock(page.name, page)
      }
    }
    body = null
    page = null
  }
}

function addRoute(route) {
  if (route.startsWith('(')) {
    var rparts = route.split(')')
    var gate = rparts[0].substring(1).trim()
    var dest = rparts[1].trim()
    option.addRoute(gate, dest)
  } else {
    option.addRoute(null, route)
  }
}

function command(s) {
  var c = s.charAt(0)
  var tail = s.substring(1).trim()
  switch(c) {
    case ':': case '!': case '[': // : = main page, ! = end page, [ = item page]
      flushItem()
      flushOption()
      flushBlock()
      collectingItem = (c=='[')
      if (c == '[' && tail.endsWith(']')) {
        tail = tail.substring(0,tail.length-1)
      }
      page = new cyan.Block(tail)
      if ( c == '!') {
        option = new cyan.Option('Try Again')
        option.addRoute(null, cyan.START)
        option.addEffect('!', '*')
      }
      mode = BODY
      body = ''
      break
    case '+': case '-': case '@': case '*': // New Effect
      if (option) {
        option.addEffect(c, tail)
      } else if (page) {
        page.addEffect(c, tail)
      } else {
        console.log(`can't add effect ${c} ${tail} to unknown context`)
      }
      break
    case '>': // Add route to existing option
      addRoute(tail)
      break
    case '#': // comment in the input file, ignore this line
      break;
    default: // New Option
      flushOption()
      var value = s.trim()
      var parts = value.split('->')
      var choice = parts[0].trim()
      if (parts.length == 1) {
        parts[1] = cyan.HERE
      }
      option = new cyan.Option(choice)
      for (i = 1; i < parts.length; ++i) {
        var route = parts[i].trim()
        addRoute(route)
      }
      break
  }
}

function load(s, callback) {
  reset()
  var lines = s.split(/\r?\n/)

  for (i in lines) {
    var line = lines[i].trim()
    switch(mode) {
      case NEW:
        var debug = false
        if (line.startsWith('!')) {
          debug = true
          line = line.substring(1)
        }
        var name = line
        game = new cyan.Game(name, debug)
        mode = COMMAND
        break
      case COMMAND:
        if (line.startsWith(':')) {
          command(line.substring(1).trim())
        }
        // ignore all other lines, such as blank lines, comments and so on
        break
      case BODY:
        if (line.startsWith(':')) {
          mode = COMMAND
          command(line.substring(1).trim())
          break
        }
        body += '\n'
        body += line
        break
      case ITEM:
        if (line.startsWith(':')) {
            flushItem()
            mode = COMMAND
            command(line.substring(1).trim())
            break
        }
        callout += '\n'
        callout += line
        break
    }
  }

  flushItem()
  flushOption()
  flushBlock()
  return callback(game)
}

module.exports = load