var path = require('path');

module.exports = {
  entry: './cyan-app.js',
  output: {
    path: path.resolve(__dirname),
    filename: 'cyan.js'
  },
  optimization: {
    minimize: false
},
  module: {
    rules: [
      {
        test: /\.txt$/i,
        use: 'raw-loader',
      },
      {
        test: /\.story$/i,
        use: 'raw-loader',
      },
    ],
  },
};